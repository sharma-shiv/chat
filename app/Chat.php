<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Chat extends Model
{
    protected $fillable = array('from', 'to', 'msg');

    // Or this,  (Read the documentation first before you use it/mass assignment)
    protected $guarded = array();

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getChats() {
        return $this->select('*')->join('users', 'users.id', 'chats.to')->where('chats.to', Auth::user()->id)->where('chats.from', '<>', Auth::user()->id)->get();
    }
}
