@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ucwords(Auth::user()->user_type) }} Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <div> 
                        <h3>History</h3>
                        <table border="1" class="table table-border">
                            <tr>
                                <th>Sr. no</th>  
                                <th>From</th>
                                <th>Message</th>  
                            </tr>
                        @forelse($chat as $c)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$c->fname." ".$c->lname}}</td>
                                <td>{{$c->msg}}</td>
                        @empty
                            <tr><td colspan="3">No records exist</td></tr>
                        @endforelse
                        </table>
                    </div>
                    <div>
                        <h3>Send Message</h3> 
                        <form action="{{ route('chat.save') }}" method="post" >
                            {{ csrf_field() }}
                            <label>Select User</label>
                            <select name="to" class="form-control">
                                <option value="">Select User</option>
                                @foreach($users as $user)
                                <option value="{{$user['id']}}">{{$user['fname']." ".$user['lname']}}</option>
                                @endforeach
                            </select>
                            <label>Type Message</label>
                            <textarea name="msg"  class="form-control"></textarea><br>
                            <input type="submit" name="submit" value="Submit"  class="btn btn-success" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
